#define CATCH_CONFIG_MAIN


#include "../catch.hpp"
#include "../src/parser.hpp"
#include "../src/text_utilities.hpp"


#define TEST_MAIN
#ifdef TEST_MAIN
#define main __
#include "../src/main.cpp"
#undef main
#endif


TEST_CASE("test do_main", "[do_main]")
{
    std::stringstream sin;
    std::stringstream sout;

    SECTION("works correctly with both stream options")
    {
        char arg[][50] =  {
                "exec_name",
                "-m",
                "4",
                "-n",
                "3",
                "../test/in1.txt",
                "-",
                "../test/in2.txt"
        };

        SECTION("file stream")
        {
            SECTION("#1")
            {
                optind = 1;
                errno = 0;

                char * argv[] = {arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], nullptr};

                sin.str("some text that shouldn't affect anything");

                REQUIRE(do_main(6, argv, sin, sout) == 0);
                REQUIRE(sout.str() == "submarine yellow submarine (4)\nyellow submarine yellow (4)\n");
            }

            SECTION("#2")
            {
                optind = 1;
                errno = 0;

                char * argv[] = {arg[0], arg[1], arg[2], arg[3], arg[4], arg[7], nullptr};

                sin.str("some text that shouldn't affect anything");

                REQUIRE(do_main(6, argv, sin, sout) == 0);
                REQUIRE(sout.str() == "be, let it (6)\nit be, let (6)\nlet it be, (6)\nlet it be (4)\n");
            }
        }

        SECTION("default stream")
        {
            SECTION("#1")
            {
                optind = 1;
                errno = 0;

                char * argv[] = {arg[0], arg[1], arg[2], arg[3], arg[4], arg[6], nullptr};

                sin.str("we all live in a yellow submarine\n"
                        "yellow submarine yellow submarine\n"
                        "we all live in a yellow submarine\n"
                        "yellow submarine yellow submarine");

                REQUIRE(do_main(6, argv, sin, sout) == 0);
                REQUIRE(sout.str() == "submarine yellow submarine (4)\nyellow submarine yellow (4)\n");
            }

            SECTION("#2")
            {
                optind = 1;
                errno = 0;

                char * argv[] = {arg[0], arg[1], arg[2], arg[3], arg[4], arg[6], nullptr};

                sin.str("let it be, let it be, let it be, let it be\n"
                        "Yeah, there will be an answer, let it be\n"
                        "let it be, let it be, let it be, let it be\n"
                        "Whisper words of wisdom, let it be");

                REQUIRE(do_main(6, argv, sin, sout) == 0);
                REQUIRE(sout.str() == "be, let it (6)\nit be, let (6)\nlet it be, (6)\nlet it be (4)\n");
            }
        }
    }
}

TEST_CASE("test parse_arguments", "[parse_arguments]")
{
    char arg[][50] =  {
            "exec_name",                        // 0
            "-m",                               // 1
            "-n",                               // 2
            "4",                                // 3
            "3",                                // 4
            "99999999999999999999999999999999", // 5
            "-42",                              // 6
            "input.txt",                        // 7
            "-",                                // 8
            "--help"                            // 9
    };

    char const opt_string[] = ":n:m:";

    GlobalArgs_t globalArgs;

    SECTION("normal args")
    {
        SECTION("no args")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], nullptr};

            REQUIRE_NOTHROW(globalArgs = parser::parse_arguments(1, argv, opt_string));

            REQUIRE(globalArgs.n_occurrences == 2);
            REQUIRE(globalArgs.n_words == 2);
            REQUIRE(globalArgs.file_name == nullptr);
        }

        SECTION("minus")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[8], nullptr};

            REQUIRE_NOTHROW(globalArgs = parser::parse_arguments(2, argv, opt_string));

            REQUIRE(globalArgs.n_occurrences == 2);
            REQUIRE(globalArgs.n_words == 2);
            REQUIRE(globalArgs.file_name == arg[8]);
        }

        SECTION("all arguments")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[1], arg[3], arg[2], arg[4], arg[7], nullptr};

            REQUIRE_NOTHROW(globalArgs = parser::parse_arguments(6, argv, opt_string));

            REQUIRE(globalArgs.n_occurrences == 4);
            REQUIRE(globalArgs.n_words == 3);
            REQUIRE(globalArgs.file_name == arg[7]);
        }
    }

    SECTION("abnormal args")
    {
        SECTION("missing argument")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[7], arg[2], arg[4], arg[1], nullptr};

            REQUIRE_THROWS_AS(globalArgs = parser::parse_arguments(5, argv, opt_string), std::runtime_error & e);
        }

        SECTION("negative value")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[1], arg[6], arg[2], arg[4], arg[7], nullptr};

            REQUIRE_THROWS_AS(globalArgs = parser::parse_arguments(5, argv, opt_string), std::runtime_error & e);
        }

        SECTION("big value")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[2], arg[5], arg[1], arg[4], arg[7], nullptr};

            REQUIRE_THROWS_AS(globalArgs = parser::parse_arguments(5, argv, opt_string), std::runtime_error & e);
        }
        SECTION("unknown arg")
        {
            optind = 1;
            errno = 0;

            char * argv[] = {arg[0], arg[1], arg[3], arg[9], arg[2], arg[4], arg[7], nullptr};

            REQUIRE_THROWS_AS(globalArgs = parser::parse_arguments(5, argv, opt_string), std::runtime_error & e);
        }
    }
}

TEST_CASE("test text::phrases reader")
{
    SECTION("accepts empty input")
    {
        SECTION("empty stream")
        {
            std::stringstream is;
            std::stringstream os;

            for (auto const & phrase : text::phrases(is, 2))
            {
                os << phrase << std::endl;
            }

            REQUIRE(os.str().empty());
        }

        SECTION("zero length phrase")
        {
            std::stringstream is;
            std::stringstream os;

            is.str("zero length phrase");

            for (auto const & phrase : text::phrases(is, 0))
            {
                os << phrase << std::endl;
            }

            REQUIRE(os.str().empty());
        }

        SECTION("both of the above")
        {
            std::stringstream is;
            std::stringstream os;

            for (auto const & phrase : text::phrases(is, 0))
            {
                os << phrase << std::endl;
            }

            REQUIRE(os.str().empty());
        }
    }

    SECTION("input words count equals to phrase length")
    {
        SECTION("#1")
        {
            std::stringstream is;
            std::stringstream os;

            is.str("input words count equals to phrase length");

            for (auto const & phrase : text::phrases(is, 7))
            {
                os << phrase << std::endl;
            }

            REQUIRE(os.str() == "input words count equals to phrase length\n");
        }

        SECTION("#2")
        {
            std::stringstream is;
            std::stringstream os;

            is.str("input");

            for (auto const & phrase : text::phrases(is, 1))
            {
                os << phrase << std::endl;
            }

            REQUIRE(os.str() == "input\n");
        }
    }

    SECTION("normal input")
    {
        std::stringstream is;
        std::stringstream os;

        is.str("Is this the price I'm paying for my past mistakes?");

        for (auto const & phrase : text::phrases(is, 7))
        {
            os << phrase << std::endl;
        }

        REQUIRE(os.str() == "Is this the price I'm paying for\n"
                            "this the price I'm paying for my\n"
                            "the price I'm paying for my past\n"
                            "price I'm paying for my past mistakes?\n");
    }
}
