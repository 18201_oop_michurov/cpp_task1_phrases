#include <getopt.h>
#include <climits>


#include "parser.hpp"


GlobalArgs_t parser::parse_arguments(
        int argc,
        char **argv,
        char const * opt_string)
{
    GlobalArgs_t global_args;

    char err_message[256] = { 0 };
    char * endptr;
    int arg;

    opterr = 0;

    while ((arg = getopt(argc, argv, opt_string)) != -1)
    {
        switch (arg)
        {
            case 'n':

                global_args.n_words = strtol(optarg, &endptr, 10u);

                if (*endptr != 0)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\nCannot convert %s to integer\n",
                            optopt,
                            optarg,
                            optarg);

                    throw std::runtime_error(err_message);
                }

                if (global_args.n_words < 0)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\nParameter values must be non-negative\n",
                            optopt,
                            optarg);

                    throw std::runtime_error(err_message);
                }

                if (errno == ERANGE)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\n%s exceeds maximum allowed value %ld\n",
                            optopt,
                            optarg,
                            optarg,
                            LONG_MAX);

                    throw std::runtime_error(err_message);
                }

                break;
            case 'm':

                global_args.n_occurrences = strtol(optarg, &endptr, 10u);

                if (*endptr != 0)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\nCannot convert %s to integer\n",
                            optopt,
                            optarg,
                            optarg);

                    throw std::runtime_error(err_message);
                }

                if (global_args.n_occurrences < 0)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\nParameter values must be non-negative\n",
                            optopt,
                            optarg);

                    throw std::runtime_error(err_message);
                }

                if (errno == ERANGE)
                {
                    sprintf(
                            err_message,
                            "Error: -%c %s\n%s exceeds maximum allowed value %ld\n",
                            optopt,
                            optarg,
                            optarg,
                            LONG_MAX);

                    throw std::runtime_error(err_message);
                }

                break;
            case ':':

                sprintf(
                        err_message,
                        "Error: -%c\nOption is missing an argument\n",
                        optopt);

                throw std::runtime_error(err_message);
            case '?':
            default:

                sprintf(
                        err_message,
                        "Error: -%c\nUnknown option\n",
                        optopt);

                throw std::runtime_error(err_message);
        }
    }

    global_args.file_name = argv[optind];

    return global_args;
}
