#include <iostream>
#include <fstream>
#include <cstring>


#include "parser.hpp"
#include "text_utilities.hpp"
#include "filter.hpp"


inline int do_main(
        int argc,
        char * argv[],
        std::istream & in,
        std::ostream & out)
{
    GlobalArgs_t global_args;

    char const opt_string[] = ":n:m:";

    try
    {
        global_args = parser::parse_arguments(argc, argv, opt_string);
    }
    catch (std::runtime_error & exception)
    {
        std::cout << exception.what() << std::endl;
        return 1;
    }

    std::map<std::string, size_t> n_occurrences;
    std::ifstream is;

    if (global_args.file_name != nullptr and strcmp("-", global_args.file_name) != 0)
    {
        is.open(global_args.file_name);

        if (not is.good())
        {
            out << "Error: unable to open input file \"" << global_args.file_name << "\"" << std::endl;
            return 0;
        }
    }

    for (auto const & phrase : text::phrases(is.is_open() ? is : in, global_args.n_words))
    {
        n_occurrences[phrase]++;
    }

    auto filtered = filter::filter_map(
            n_occurrences,
            [&global_args](std::pair<std::string, size_t> const & pair)
            {
                return pair.second >= global_args.n_occurrences;
            }
    );

    std::sort(
            filtered.begin(),
            filtered.end(),
            [](std::pair<std::string, size_t> const & p1, std::pair<std::string, size_t> const & p2)
            {
                return p1.second > p2.second;
            }
    );

    for (auto const & pair : filtered)
    {
        out << pair.first << " (" << pair.second << ")" << std::endl;
    }

    return 0;
}


int main(
        int argc,
        char * argv[])
{
    return do_main(argc, argv, std::cin, std::cout);
}