#include <algorithm>
#include <functional>
#include <utility>


#include "text_utilities.hpp"


using namespace text;


phrases::iterator phrases::begin()
{
    return {this->istream, this->phrase_length, not bool(this->phrase_length)};
}


phrases::iterator phrases::end()
{
    return {this->istream, this->phrase_length, true};
}


phrases::phrases(
        std::istream & istream,
        size_t phrase_length)
        :
        istream(istream),
        phrase_length(phrase_length)
{}


phrases::iterator::iterator(
        std::istream &istream,
        size_t  phrase_length,
        bool eof)
        :
        istream(istream),
        phrase_length(phrase_length),
        eof(eof)
{
    if (this->eof)
    {
        return;
    }

    std::string word;
    for (size_t read = 0; read < this->phrase_length and this->istream >> word; ++read)
    {
        this->current_phrase.append(" ").append(word);
        this->word_size.push(word.size());
    }

    this->current_phrase.erase(0, 1);
    this->eof = this->istream.eof() and this->word_size.size() != this->phrase_length;
}


phrases::iterator & phrases::iterator::operator++()
{
    std::string word;

    this->eof = not (this->istream >> word);

    if (not this->eof)
    {
        this->current_phrase.append(" ").append(word);
        this->word_size.push(word.size());

        this->current_phrase.erase(0, this->word_size.front() + 1);
        this->word_size.pop();
    }

    return *this;
}


std::string phrases::iterator::operator*()
{
    return this->current_phrase;
}


bool phrases::iterator::operator!=(
        const phrases::iterator &other)
{
    return this->eof != other.eof or this->istream.rdbuf() != other.istream.rdbuf();
}
