#ifndef SPLITTER_TEXT_UTILITIES_HPP
#define SPLITTER_TEXT_UTILITIES_HPP


#include <string>
#include <iostream>
#include <queue>


namespace text
{
    class phrases
    {
    public:
        class iterator;

        iterator begin();

        iterator end();

        explicit phrases(
                std::istream & istream,
                size_t phrase_length);

    private:
        std::istream & istream;
        size_t phrase_length;
    };


    class phrases::iterator : std::iterator<std::input_iterator_tag, std::string>
    {
        friend phrases;
    private:
        iterator(
                std::istream & istream,
                size_t phrase_length,
                bool eof);

    public:
        iterator & operator++();

        std::string operator*();

        bool operator!=(iterator const & other);

    private:
        std::queue<size_t> word_size;
        std::string current_phrase;
        std::istream & istream;
        size_t phrase_length;
        bool eof;
    };
}


#endif //SPLITTER_TEXT_UTILITIES_HPP
