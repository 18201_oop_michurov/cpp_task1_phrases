#ifndef CPP_TASK1_PHRASES_FILTER_HPP
#define CPP_TASK1_PHRASES_FILTER_HPP


#include <map>
#include <vector>
#include <algorithm>


namespace filter
{
    template <typename Key, typename Value, typename UnaryPredicate>
    std::vector<std::pair<Key, Value>> filter_map(
            std::map<Key, Value> const & map,
            UnaryPredicate predicate)
    {
        std::vector<std::pair<Key, Value>> result(map.size());

        auto it = std::copy_if(map.begin(), map.end(), result.begin(), predicate);

        result.resize(unsigned(std::distance(result.begin(), it)));

        return result;
    }
}


#endif //CPP_TASK1_PHRASES_FILTER_HPP
