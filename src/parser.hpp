#ifndef CPP_TASK1_PHRASES_PARSER_H
#define CPP_TASK1_PHRASES_PARSER_H


#include <iostream>


struct GlobalArgs_t
{
    long n_words = 2;
    long n_occurrences = 2;
    char * file_name = nullptr;
};


#define UNDERFLOW       (-1)
#define OVERFLOW        (-2)
#define NOT_A_NUMBER    (-3)
#define UNKNOWN         (-4)
#define MISSING         (-5)


namespace parser
{
    GlobalArgs_t parse_arguments(
            int argc,
            char **argv,
            char const * opt_string);

}


#endif //CPP_TASK1_PHRASES_PARSER_H
