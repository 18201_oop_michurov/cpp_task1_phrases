# **Phrase Counter**

Простое консольное приложение, подсчитывающее количество фраз заданной 
длины в тексте.
    
# Использование

    $> phrases [-m M] [-n N] [<filename>|-]

**Параметры:**
*  -n - (опционально) длина фразы (в словах) (по умолчанию 2).
*  -m - (опционально) минимальное количество повторов, при котором фраза будет выведена на экран (по умолчанию 2).
*  filename - (опционально) имя файла, из которого должно производиться чтение. Если не указано или имеет значение "-", ввод происходит из стандартного потока ввода.

**Примеры:**

1.

    $> phrases -n 3 -m 4 -
    we all live in a yellow submarine
    yellow submarine yellow submarine
    we all live in a yellow submarine
    yellow submarine yellow submarine
    submarine yellow submarine (4)
    yellow submarine yellow (4)
2.

    $> phrases -
    In the town where I was born
    Lived a man who sailed to sea
    And he told us of his life
    In the land of submarines
    In the (2)